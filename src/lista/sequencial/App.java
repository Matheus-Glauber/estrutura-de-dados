package lista.sequencial;

public class App {

	public static void main(String[] args) {

		ListaSequencial lista = new ListaSequencial();

		Pessoa p1 = new Pessoa("Matheus");
		Pessoa p2 = new Pessoa("Ingrid");
		Pessoa p3 = new Pessoa("Amon");

		lista.adiciona(p1);
		lista.adiciona(p2);
//		lista.adiciona(p3);
//		lista.adiciona(p1);
//		lista.adiciona(p2);
//		lista.adiciona(p3);

		lista.adicionaNaPosicao(60, p3);
//		lista.adicionaNaPosicao(1, p2);
//		lista.adicionaNaPosicao(2, p3);
//		lista.adicionaNaPosicao(3, p1);

		System.out.println(lista.verificaVazia());
		System.out.println(lista.verificaCheia());
		System.out.println(lista.tamanhoLista());
		System.out.println(lista.toString());
		 
	}

}
