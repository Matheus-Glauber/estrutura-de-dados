package lista.sequencial;

public class ListaSequencial {
	
	private int tamanhoArray = 3;
	private int elementosNaLista = 0;
	private Object[] listaGenerica = new Object[tamanhoArray];
	
	public String toString() {
		if(this.elementosNaLista == 0) {
			return "[]";
		}
		
		StringBuilder builder = new StringBuilder();
		builder.append("[ ");
		
		for (int i = 0; i < listaGenerica.length; i++) {
			builder.append(this.listaGenerica[i]);
			builder.append(", ");			
		}
		builder.append("]");
		
		return builder.toString();
	}
	
	public boolean verificaVazia() {
		boolean cond = false;
		for (int i = 0; i < listaGenerica.length; i++) {
			if(listaGenerica[i] != null) {
				cond=false;
				break;
			}else {
				cond=true;
			}
		}
		return cond;
	}
	
	public boolean verificaCheia() {
		boolean cond = false;
		for (int i = 0; i < listaGenerica.length; i++) {
			if(listaGenerica[i] != null) {
				cond=true;
			}else {
				cond=false;
				break;
			}
		}
		return cond;
	}
	
	public int tamanhoLista() {
		return listaGenerica.length;
	}
	
	public void adiciona(Object obj) {
		if(verificaCheia() == false) {
			this.listaGenerica[this.elementosNaLista] = obj;
			this.elementosNaLista ++;
		}else {
			aumentarLimiteLista();
			this.listaGenerica[this.elementosNaLista] = obj;
			this.elementosNaLista ++;
		}
		
	}
	
	public void adicionaNaPosicao(int posicao, Object obj) {
		if(!this.posicaoValida(posicao)) {
			throw new IllegalArgumentException("Posi��o Inv�lida");
		}
		if(listaGenerica[posicao] == null) {
			if(verificaCheia() == false) {
				adicionarNaPosicaoImplement(posicao, obj);
			}else {
				aumentarLimiteLista();
				adicionarNaPosicaoImplement(posicao, obj);
			}		
		}else {
			adicionarNaPosicaoImplement(posicao, obj);
		}
	}
	
	private void adicionarNaPosicaoImplement(int posicao, Object obj) {
		this.aumentarLimiteLista1();
		for (int i = this.elementosNaLista; i >= posicao; i-=1) {
			this.listaGenerica[i+1] = this.listaGenerica[i];
			
		}
		this.listaGenerica[posicao] = obj;
		this.elementosNaLista ++;
	}
	
	private boolean posicaoValida(int posicao) {
		return posicao >= 0 && posicao <= this.listaGenerica.length;
	}
	
	private void aumentarLimiteLista() {
		tamanhoArray = tamanhoArray * 2;
		Object[] temp = new Object[tamanhoArray];
		for (int i = 0; i < listaGenerica.length; i++) {
			temp[i] = listaGenerica[i];
		}
		listaGenerica = temp;
	}
	
	private void aumentarLimiteLista1() {
		tamanhoArray = tamanhoArray + 1;
		Object[] temp = new Object[tamanhoArray];
		for (int i = 0; i < listaGenerica.length; i++) {
			temp[i] = listaGenerica[i];
		}
		listaGenerica = temp;
	}
}
